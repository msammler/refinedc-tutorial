all: check_all
.PHONY: all

C_SRC = $(wildcard examples/*.c) $(wildcard examples/VerifyThis2021/*.c) $(wildcard tutorial/*.c) $(wildcard tutorial/VerifyThis/*.c)

%.c.gen: %.c phony
	opam exec -- refinedc check $<
.PHONY: phony

check_all: $(addsuffix .gen, $(C_SRC))
.PHONY: check_all


update-files:
	./update_files.sh
.PHONY: update-files

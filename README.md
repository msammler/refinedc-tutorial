# RefinedC Tutorial

This repository contains the tutorial files and examples from
[RefinedC](https://gitlab.mpi-sws.org/iris/refinedc).

To install RefinedC, please follow the instructions in the [RefinedC
README](https://gitlab.mpi-sws.org/iris/refinedc/-/blob/master/README.md).


To check whether RefinedC is installed correctly, please clone this repository:
```
git clone git-rts@gitlab.mpi-sws.org:msammler/refinedc-tutorial.git
```
Change into it:
```
cd refinedc-tutorial
```
Update the opam environment:
```
eval $(opam env)
```
Run RefinedC:
```
refinedc check tutorial/t01_basic.c
```
If you see the following message, RefinedC was installed correctly:
```
File "tutorial/t01_basic.c" successfully checked.
```

This tutorial is work in progress. You can either start by looking at
[tutorial/t01_basic.c](tutorial/t01_basic.c) or
[tutorial/VerifyThis/t01_refinements.c](tutorial/VerifyThis/t01_refinements.c).

The files in this repository correspond to commit [ef732d956054a4c8b6346e8ec08e3e83c58f719a](https://gitlab.mpi-sws.org/iris/refinedc/-/tree/ef732d956054a4c8b6346e8ec08e3e83c58f719a) of RefinedC.

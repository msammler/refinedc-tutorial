#!/bin/bash

set -e

FOLDERS="tutorial examples"
COMMIT=$(curl -s "https://gitlab.mpi-sws.org/api/v4/projects/1724/repository/commits?ref_name=master" | jq -r ".[0].id")
echo "Updating to commit $COMMIT."

rm -rf ${FOLDERS}

wget -q "https://gitlab.mpi-sws.org/iris/refinedc/-/archive/$COMMIT/refinedc-$COMMIT.tar.gz" -O "refinedc.tar.gz"

tar xf "refinedc.tar.gz"

for f in ${FOLDERS}; do
    mv "refinedc-$COMMIT/${f}" "${f}"
done

rm -rf refinedc.tar.gz "refinedc-$COMMIT"

sed -i "s/[a-f0-9]\\{40\\}/$COMMIT/g" README.md
